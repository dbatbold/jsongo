package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
)

func main() {

	dec := json.NewDecoder(os.Stdin)
	var v interface{}
	if err := dec.Decode(&v); err != nil {
		log.Fatal("Error:", err)
	}
	//fmt.Println("%v", v)

	if data, err := json.MarshalIndent(&v, "", "  "); err != nil {
		log.Fatal("Error:", err)
	} else {
		fmt.Println(string(data))
	}

}
